﻿using System;
namespace confdsharp.Maagic
{
    public class NodeListElement : Node
    {
        private string[] _keys;
        public string[] getKeys() {
            return _keys;
        }
        public NodeListElement(confd_cs_node node, string [] keys) : base(node) {
            _keys = keys;
        }

        public override string GetNodePathElement() {
            return parent.name + "{" + String.Join(',', _keys) + "}";
        }
    }
}
