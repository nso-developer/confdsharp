﻿using System;
namespace confdsharp.Maagic
{
    public class NodeList : Node
    {
        public NodeList(confd_cs_node node) : base(node) {
        }

        public override string GetNodePathElement() {
            return ""; //Empty because child element will include parent's name
        }
    }
}
