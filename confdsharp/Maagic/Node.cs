﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Dynamic;
using System.Reflection;
using System.Runtime.InteropServices;

namespace confdsharp.Maagic
{
    public class Node : DynamicObject, IEquatable<Node>, IEnumerable //https://stackoverflow.com/questions/46948289/how-do-you-convert-any-c-sharp-object-to-an-expandoobject
    {                                               //https://stackoverflow.com/questions/3874875/c-sharp-4-0-dynamic-inheriting-from-dynamicobject
        public Node parent;
        
        public string name;
        public int ns;
        public string ns_prefix;
        public int tag;
        public List<Node> children = new List<Node>();
        private object _populate_lock = new object();
        protected bool populated = false;
        protected confd_cs_node cs_node;
        private string _path;
        public string path {
            get {
                if (String.IsNullOrEmpty(_path)){
                    _path = GetPath();
                }
                return _path;
            }
            set {
                _path = value;
            }
        }

        public string NodePathElement {
            get {
                if (String.IsNullOrEmpty(_path)) {
                    _path = GetNodePathElement();
                }
                return _path;
            }
            
        }

        public virtual string GetNodePathElement() {
            return name;
        }
       

        public string GetPath() {
            List<string> pathelements = new List<string>();
            pathelements.Add(NodePathElement);
            var pointer = parent;
            while(pointer != null) {
                pathelements.Add(pointer.NodePathElement);
                pointer = pointer.parent;
            }
            pathelements.Reverse();
            return "/" + String.Join('/', pathelements);

        }

        public dynamic c { //c for children
            get {
                return (dynamic)this;
            }
        }

        public virtual string shortName {
            get {
                return this.ns_prefix + ":" + this.name;
            }
        }



        public Node(string path, string name, int ns = 0, int tag = 0, Node parent=null) {
            this.path = path;
            this.name = name;
            this.parent = parent;
            this.ns = ns;
            this.tag = tag;
            this.ns_prefix = libconfd.confd_ns2prefix_w((uint)this.ns);
            //TODO: get or make a confd_cs_node cs_node HERE !!?
        }

        public Node(confd_cs_node node) {

            this.name = libconfd.confd_hash2str_w(node.tag);
            this.ns = (int)node.ns;
            this.ns_prefix = libconfd.confd_ns2prefix_w((uint)this.ns);
            this.tag = (int)node.tag;
            this.cs_node = node;
        }

        public override string ToString() {
            return this.shortName + " - " + this.path; 
        }

       

        public void populate(bool force = false) {
            lock (_populate_lock) {
                if (populated && !force) { return; }
                var next = cs_node.children;
                if (next != null && (Int64)next > (Int64)0) {
                    confd_cs_node node = Marshal.PtrToStructure<confd_cs_node>(next);
                    addChild(NodeFactory.CreateNode(node));
                    while (node.next != null && (Int64)node.next > (Int64)0) {
                        node = Marshal.PtrToStructure<confd_cs_node>(node.next);
                        addChild(NodeFactory.CreateNode(node));
                    }
                }

            }
            populated = true;
            
        }
        

        public void addChild(Node child) {
            child.parent = this;
            children.Add(child);
        }
        
        public bool Equals([AllowNull] Node other) {
            throw new NotImplementedException();
        }

        public IEnumerator GetEnumerator() {
            if(!populated) { populate(); }
            return children.GetEnumerator();
        }

        public dynamic this[string name] {
            get {
                if (!populated) { populate(); }
                return children.Find(x => x.shortName==name);
            }
            set { ; }
        }

        public override bool TryGetMember( GetMemberBinder binder, out object result) {
            if (!populated) { populate(); }
            // Converting the property name to lowercase
            // so that property names become case-insensitive.
            string name = objNameToShortName(binder.Name);

            // If the property name is found in a dictionary,
            // set the result parameter to the property value and return true.
            // Otherwise, return false.

            result = this[name];
            return true;
        }

        private string objNameToShortName(string objName) {
            return objName.Replace("__", ":").Replace("_", "-");
        }

        // If you try to set a value of a property that is
        // not defined in the class, this method is called.
        public override bool TrySetMember( SetMemberBinder binder, object value) {
            // Converting the property name to lowercase
            // so that property names become case-insensitive.
            this[binder.Name] = (Node)value;

            // You can always add a value to a dictionary,
            // so this method always returns true.
            return true;
        }
    }
}
