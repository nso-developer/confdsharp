﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Runtime.InteropServices;

namespace confdsharp.Maagic
{



    public class RootNode :  Node
    {
        public List<confd_nsinfo> namespaces = new List<confd_nsinfo>(); //TODO: Create a namespace class that will handle lookup and will be assigned to nodes
        Dictionary<int, confd_cs_node> rootnodes = new Dictionary<int, confd_cs_node>();
        public RootNode() : base("","root") {
            read_namespaces();
            populated = true;
        }

        private void read_namespaces() {
            IntPtr ptr = new IntPtr();
            int nscnt = libconfd.confd_get_nslist(ref ptr);
            confd_nsinfo[] ns_structs;
            helpers.MarshalUnmananagedArray2Struct<confd_nsinfo>(ptr, nscnt, out ns_structs);
            foreach(confd_nsinfo nsstruct in ns_structs) {
                namespaces.Add(nsstruct);

                var root = libconfd.confd_find_cs_root((int)nsstruct.hash);
                if (root != null && (Int64)root > (Int64)0 ) {
                    confd_cs_node node = Marshal.PtrToStructure<confd_cs_node>(root);
                    rootnodes.Add((int)nsstruct.hash, node);                    
                    addChild(new Node(node));
                    while(node.next != null && (Int64)node.next > (Int64)0) {
                        node = Marshal.PtrToStructure<confd_cs_node>(node.next);
                        addChild(new Node(node));
                    }
                }
                    
            }
        }

       
    }
}
