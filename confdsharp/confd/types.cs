﻿using System;
using System.Runtime.InteropServices;

using in_addr_t = System.UInt32;

namespace confdsharp
{
    #region structures
    /*[StructLayout(LayoutKind.Sequential)] //https://csharp.hotexamples.com/site/file?hash=0xe16a2f0ffa18a30978a5423e17eeeaf7230071d732ffc32d65a29cb3e08809dd&fullName=alfa2_acr.hak/SocketIo.cs&project=CastanoALFA/ALFA-Base-Resources
    public struct sockaddr_in
    {
        public byte sin_len;
        public byte sin_family;
        public ushort sin_port;
        public uint sin_addr;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public byte[] sin_zero;
    }; */

    public struct confd_buf
    {
        public int size;
        public string data;
    }

    public struct confd_nsinfo
    {
        public string uri;
        public string prefix;
        public UInt32 hash;
        public string revision;
        public string module;
    };


    public struct confd_cs_node_info
    {
        public IntPtr keys; //UInt32
        public int minOccurs;
        public int maxOccurs;   /* -1 if unbounded */
        public  confd_vtype shallow_type;
        public IntPtr type;//confd_type
        public IntPtr defval; //confd_value_t
        public  IntPtr choices; //confd_cs_choice
        public int flags;
        public byte cmp;
        public  IntPtr meta_data; //confd_cs_meta_data
    };  

   public struct confd_cs_meta_data
    {
        string key;
        string value;
    };

    public struct confd_cs_node
    {
        public UInt32 tag;
        public UInt32 ns;
        public confd_cs_node_info info;
        public IntPtr parent; //confd_cs_node
        public IntPtr children; //confd_cs_node
        public IntPtr next; //confd_cs_node
        public IntPtr opaque;   /* private user data */
    };

    public struct confd_cs_choice
    {
        public UInt32 tag;
        public UInt32 ns;
        public int minOccurs;
        public IntPtr default_case; //confd_cs_case
        public IntPtr parent;         /* NULL if parent is case */
        public IntPtr cases; //confd_cs_case
        public IntPtr next; //confd_cs_choice
        public IntPtr case_parent;    /* NULL if parent is node */ //confd_cs_case
    };

    struct confd_cs_case
    {
        UInt32 tag;
        UInt32 ns;
        public IntPtr first; //confd_cs_node
        public IntPtr last; //confd_cs_node
        public IntPtr parent; //confd_cs_choice
        public IntPtr next; //confd_cs_node
        public IntPtr choices; //confd_cs_choice
    };

    public struct confd_hkeypath_t
    {
        int len;
        [MarshalAs(UnmanagedType.LPArray, SizeConst = constants.MAXDEPTH * constants.MAXKEYLEN)]
        confd_value_t[][] v; //[MAXDEPTH][MAXKEYLEN]
    }

    public struct xml_tag
    {
        UInt32 tag;
        UInt32 ns;
    };



    
    [StructLayout(LayoutKind.Explicit)] //https://stackoverflow.com/questions/51618837/c-sharp-marshal-unmanaged-pointer-return-type
    public struct confd_value_t //Not used, using pointers now
    {
        [FieldOffset(0)]
        public confd_vtype vtype;

        

        [FieldOffset(8)] //Not sure why Offset is 8, enum should be Int which is 4 bytes. Maybe int defaults to int64 on mac.
        public ulong uint64_value;
        [FieldOffset(8)]
        public UInt32 uint32_value;
        [FieldOffset(8)]
        public ushort uint16_value;
        [FieldOffset(8)]
        public byte uint8_value;


        [FieldOffset(8)]
        public long int64_value;
        [FieldOffset(8)]
        public Int32 int32_value;
        [FieldOffset(8)]
        public short int16_value;
        [FieldOffset(8)]
        public sbyte int8_value;

        //[FieldOffset(8)]
        //[MarshalAs(UnmanagedType.LPArray, SizeConst = 16)]
        //public byte[] address;

        [FieldOffset(8)]
        public string string_value;




    }
    /*
    public struct confd_value_t
    {
        public confd_vtype vtype;
        public long value;
    } */

    public enum confd_vtype
    {
        C_NOEXISTS = 1,  /* end marker                              */
        C_XMLTAG = 2,  /* struct xml_tag                          */
        C_SYMBOL = 3,  /* not yet used                            */
        C_STR = 4,  /* NUL-terminated strings                  */
        C_BUF = 5,  /* confd_buf_t (string ...)                */
        C_INT8 = 6,  /* int8_t    (int8)                        */
        C_INT16 = 7,  /* int16_t   (int16)                       */
        C_INT32 = 8,  /* int32_t   (int32)                       */
        C_INT64 = 9,  /* int64_t   (int64)                       */
        C_UINT8 = 10, /* u_int8_t  (uint8)                       */
        C_UINT16 = 11, /* u_int16_t (uint16)                      */
        C_UINT32 = 12, /* u_int32_t (uint32)                      */
        C_UINT64 = 13, /* u_int64_t (uint64)                      */
        C_DOUBLE = 14, /* double (xs:float,xs:double)             */
        C_IPV4 = 15, /* struct in_addr in NBO                   */
        /*  (inet:ipv4-address)                    */
        C_IPV6 = 16, /* struct in6_addr in NBO                  */
        /*  (inet:ipv6-address)                    */
        C_BOOL = 17, /* int       (boolean)                     */
        C_QNAME = 18, /* struct confd_qname (xs:QName)           */
        C_DATETIME = 19, /* struct confd_datetime                   */
        /*  (yang:date-and-time)                   */
        C_DATE = 20, /* struct confd_date (xs:date)             */
        C_TIME = 23, /* struct confd_time (xs:time)             */
        C_DURATION = 27, /* struct confd_duration (xs:duration)     */
        C_ENUM_VALUE = 28, /* int32_t (enumeration)                   */
        C_BIT32 = 29, /* u_int32_t (bits size 32)                */
        C_BIT64 = 30, /* u_int64_t (bits size 64)                */
        C_LIST = 31, /* confd_list (leaf-list)                  */
        C_XMLBEGIN = 32, /* struct xml_tag, start of container or   */
        /*  list entry                             */
        C_XMLEND = 33, /* struct xml_tag, end of container or     */
        /*  list entry                             */
        C_OBJECTREF = 34, /* struct confd_hkeypath*                  */
        /*  (instance-identifier)                  */
        C_UNION = 35, /* (union) - not used in API functions     */
        C_PTR = 36, /* see cdb_get_values in confd_lib_cdb(3)  */
        C_CDBBEGIN = 37, /* as C_XMLBEGIN, with CDB instance index  */
        C_OID = 38, /* struct confd_snmp_oid*                  */
        /*  (yang:object-identifier)               */
        C_BINARY = 39, /* confd_buf_t (binary ...)                */
        C_IPV4PREFIX = 40, /* struct confd_ipv4_prefix                */
        /*  (inet:ipv4-prefix)                     */
        C_IPV6PREFIX = 41, /* struct confd_ipv6_prefix                */
        /*  (inet:ipv6-prefix)                     */
        C_DEFAULT = 42, /* default value indicator                 */
        C_DECIMAL64 = 43, /* struct confd_decimal64 (decimal64)      */
        C_IDENTITYREF = 44, /* struct confd_identityref (identityref)  */
        C_XMLBEGINDEL = 45, /* as C_XMLBEGIN, but for a deleted list   */
        /*  entry                                  */
        C_DQUAD = 46, /* struct confd_dotted_quad                */
        /*  (yang:dotted-quad)                     */
        C_HEXSTR = 47, /* confd_buf_t (yang:hex-string)           */
        C_IPV4_AND_PLEN = 48, /* struct confd_ipv4_prefix              */
        /*  (tailf:ipv4-address-and-prefix-length) */
        C_IPV6_AND_PLEN = 49, /* struct confd_ipv6_prefix              */
        /*  (tailf:ipv6-address-and-prefix-length) */
        C_BITBIG = 50, /* confd_buf_t (bits size > 64)            */
        C_MAXTYPE           /* maximum marker; add new values above    */
    };

    [StructLayout(LayoutKind.Sequential)]
    public struct sockaddr_in
    {
        public byte sin_len;
        public byte sin_family;
        public ushort sin_port;
        public uint sin_addr;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public byte[] sin_zero;
    };

    [StructLayout(LayoutKind.Sequential)]
    public struct confd_ip
    {
        public int af;    /* AF_INET | AF_INET6 */
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public byte[] ip;
    };

    [StructLayout(LayoutKind.Sequential)]
    public struct sockaddr
    {
        public ushort sa_len;         /* total length */
        public ushort sa_family;      /* [XSI] address family */
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 14)]
        public byte[] sa_data;    /* [XSI] addr value (actually larger) */
    };


    [StructLayout(LayoutKind.Explicit)]
    public unsafe struct in_addr
    {
        [FieldOffset(0)]
        private in_addr_t __align;
        [FieldOffset(0)]
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public byte[] s_addr;
    }



    [StructLayout(LayoutKind.Explicit)]
    public struct in6_addr
    {
        [FieldOffset(0)]
        private uint __align;
        [FieldOffset(0)]
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public byte[] s6_addr;
    }
    #endregion

    #region enums

    public enum confd_return_codes
    {
        CONFD_DELAYED_RESPONSE = 2,
        CONFD_ACCUMULATE = 1,
        CONFD_OK = 0,
        CONFD_ERR = -1,
        CONFD_EOF = -2,
        CONFD_VALIDATION_WARN = -3,
        CONFD_ALREADY_LOCKED = -4,
        CONFD_IN_USE = -5
    };

    public enum confd_debug_level
    {
        CONFD_SILENT = 0,
        CONFD_DEBUG = 1,
        CONFD_TRACE = 2,      /* trace callback calls */
        CONFD_PROTO_TRACE = 3  /* tailf internal protocol trace */
    };

    enum cdb_sock_type
    {
        CDB_READ_SOCKET,
        CDB_SUBSCRIPTION_SOCKET,
        CDB_DATA_SOCKET
    };

    /// <summary>
    /// Protocol id numbers for ALFA protocols.  The protocol byte follows
    /// the ALFA protocol magic int.
    /// </summary>
    public enum PROTOCOL_ID : byte
    {
        /// <summary>
        /// The datagram protocol.  Connectionless and without any
        /// reliability guarantees.
        /// </summary>
        PROTOCOL_DATAGRAM = 0,
    };



    public enum confd_trans_mode
    {
        CONFD_READ = 1,
        CONFD_READ_WRITE = 2
    };

    public enum confd_list_filter_type
    {
        CONFD_LF_OR = 0,
        CONFD_LF_AND = 1,
        CONFD_LF_NOT = 2,
        CONFD_LF_CMP = 3,
        CONFD_LF_EXISTS = 4,
        CONFD_LF_EXEC = 5
    };

    public enum confd_proto
    {
        CONFD_PROTO_UNKNOWN = 0,
        CONFD_PROTO_TCP = 1,
        CONFD_PROTO_SSH = 2,
        CONFD_PROTO_SYSTEM = 3,  /* ConfD initiated transactions */
        CONFD_PROTO_CONSOLE = 4,
        CONFD_PROTO_SSL = 5,
        CONFD_PROTO_HTTP = 6,
        CONFD_PROTO_HTTPS = 7,
        CONFD_PROTO_UDP = 8 /* SNMP sessions */
    };

    public enum confd_expr_op
    {
        CONFD_CMP_NOP = 0,
        CONFD_CMP_EQ = 1,
        CONFD_CMP_NEQ = 2,
        CONFD_CMP_GT = 3,
        CONFD_CMP_GTE = 4,
        CONFD_CMP_LT = 5,
        CONFD_CMP_LTE = 6,
        /* functions below */
        CONFD_EXEC_STARTS_WITH = 7,
        CONFD_EXEC_RE_MATCH = 8,
        CONFD_EXEC_DERIVED_FROM = 9,
        CONFD_EXEC_DERIVED_FROM_OR_SELF = 10
    };

    public enum confd_dbname
    {
        CONFD_NO_DB = 0,
        CONFD_CANDIDATE = 1,
        CONFD_RUNNING = 2,
        CONFD_STARTUP = 3,
        CONFD_OPERATIONAL = 4,
        CONFD_TRANSACTION = 5,   /* trans_in_trans */
        CONFD_PRE_COMMIT_RUNNING = 6,
        CONFD_INTENDED = 7
    };



    #endregion
}
