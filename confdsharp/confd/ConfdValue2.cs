﻿using System;
using System.Net;
using System.Runtime.InteropServices;

/**     /\
 *     /  \
 *    / !! \
 *   /______\
 * 
 *    THIS IS NOT USED. LEAVING FOR REFERENCE
 * 
 * 
 * 
 * 
 */
namespace confdsharp.confd
{
    public class ConfdValue2
    {
        private confd_value_t confd_value;
        public dynamic Value {
            get {
                return this.getValue();
            } set {
                this.setValue(value);
            }
        }
        public confd_value_t ValueStruct {
            get {
                return this.getValueAsConfdValue();
            }
        }
        public ConfdValue2(dynamic value) {
            this.setValue(value);
           
        }

        public ConfdValue2() {
        }


        /*
[StructLayout(LayoutKind.Explicit)] //https://stackoverflow.com/questions/51618837/c-sharp-marshal-unmanaged-pointer-return-type
public struct confd_value_t //https://stackoverflow.com/questions/1498931/marshalling-array-of-strings-to-char-in-c-sharp
{
   [FieldOffset(0)]
   public confd_vtype vtype;  // as defined above 

//struct xml_tag xmltag;
[FieldOffset(4)]
   uint symbol;
   //confd_buf_t buf;
   //confd_buf_const_t c_buf;
   [FieldOffset(4)]
   string s; //char* = string ?

   [FieldOffset(4)]
   string c_s; //const char*

   [FieldOffset(4)]
   byte i8;

   [FieldOffset(4)]
   short i16;

   [FieldOffset(4)]
   int i32;
   //int64_t i64;
   //u_int8_t u8;
   //u_int16_t u16;
   //u_int32_t u32;
   //u_int64_t u64;
   //double d;

   //[FieldOffset(4)]
   //struct in_addr ip;
   //[FieldOffset(4)]
   //struct in6_addr ip6;
   //int boolean;
   //struct confd_qname qname;
   //struct confd_datetime datetime;
   //struct confd_date date;
   //struct confd_time time;
   //struct confd_duration duration;
   //int32_t enumvalue;
   //u_int32_t b32;
   //u_int64_t b64;
   //struct confd_list list;
   //struct confd_hkeypath *hkp;
   //struct confd_vptr ptr;
   //struct confd_snmp_oid *oidp;
   //struct confd_ipv4_prefix ipv4prefix;
   //struct confd_ipv6_prefix ipv6prefix;
   //struct confd_decimal64 d64;
   //struct confd_identityref idref;
   //struct confd_dotted_quad dquad;
   //DO not do this one //u_int32_t enumhash;     // backwards compat 


} */
        private void setValue ( dynamic value) {
            if (value is IPAddress) {
                if (((IPAddress)value).AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork) {
                    setIPV4Value(value);
                } else {
                    setIPV6Value(value);
                }

                
            } else if (value is sbyte) {
                this.confd_value.int8_value = value;
                this.confd_value.vtype = confd_vtype.C_INT8;
            } else if (value is int || value is Int32) {
                this.confd_value.int32_value = value;
                this.confd_value.vtype = confd_vtype.C_INT32;
            } else if (value is short) {
                this.confd_value.int16_value = value;
                this.confd_value.vtype = confd_vtype.C_INT16;

            } else if (value is long) {
                this.confd_value.int64_value = value;
                this.confd_value.vtype = confd_vtype.C_INT64;

            } else if (value is byte) {
                this.confd_value.uint8_value = value;
                this.confd_value.vtype = confd_vtype.C_UINT8;
            } else if (value is uint || value is UInt32) {
                this.confd_value.int32_value = value;
                this.confd_value.vtype = confd_vtype.C_UINT32;
            } else if (value is ushort) {
                this.confd_value.int16_value = value;
                this.confd_value.vtype = confd_vtype.C_UINT16;

            } else if (value is ulong) {
                this.confd_value.int64_value = value;
                this.confd_value.vtype = confd_vtype.C_UINT64;

            } else if (value is string) {
               // this.confd_value.string_value = value;
                this.confd_value.vtype = confd_vtype.C_STR;

            } else if (value is confd_value_t) {
                this.confd_value = value;
            } else {
                throw new NotImplementedException();
            }

        }
        private dynamic getValue() {
            /*
             * /Users/xxx/NSO/4.7.5/src/ncs/pyapi/src/types.c confdValue_init:596
             * check confd_value_t *eterm_to_val(const ETERM *term, confd_value_t *v) confd_internal.c:170
             */
            switch (confd_value.vtype) { 
                case confd_vtype.C_IPV4:
                    return getIPV4Value();
                case confd_vtype.C_IPV6:
                    return getIPV6Value();
                case confd_vtype.C_UINT8:
                    return confd_value.uint8_value;
                case confd_vtype.C_UINT16:
                    return confd_value.uint16_value;
                case confd_vtype.C_UINT32:
                    return confd_value.uint32_value;
                case confd_vtype.C_UINT64:
                    return confd_value.uint64_value;
                case confd_vtype.C_INT8:
                    return confd_value.int8_value;
                case confd_vtype.C_INT16:
                    return confd_value.int16_value;
                case confd_vtype.C_INT32:
                    return confd_value.int32_value;
                case confd_vtype.C_INT64:
                    return confd_value.int64_value;
                case confd_vtype.C_STR:
                    //return confd_value.string_value;
                default:
                    throw new NotImplementedException();
            }
        }



        public confd_value_t getValueAsConfdValue() {
            return confd_value;
        }

        private IPAddress getIPV4Value() {
            var ip = new IPAddress(confd_value.int32_value);
            return ip;
        }

        private IPAddress getIPV6Value() {
            throw new NotImplementedException();
            //var ip = new IPAddress(confd_value.ipv6.address);//TODO Maybe we need reverse here
            //return ip;
        }

        private void setIPV4Value ( IPAddress ipv4) {
            confd_value = new confd_value_t();
            confd_value.vtype = confd_vtype.C_IPV4;
            this.confd_value.uint32_value = (UInt32)ipv4.Address;


        }

        private void setIPV6Value(IPAddress ipv6) {
            confd_value = new confd_value_t();
            confd_value.vtype = confd_vtype.C_IPV6;
            //this.confd_value.ipv6.address = ipv6.GetAddressBytes(); //TODO Maybe we need reverse here


        }
    }
}
