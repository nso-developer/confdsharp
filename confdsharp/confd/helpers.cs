﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using confdsharp.confd;

namespace confdsharp
{
    public class helpers
    {
        public helpers() {
        }

        public static void MarshalUnmananagedArray2Struct<T>(IntPtr unmanagedArray, int length, out T[] mangagedArray) {
            var size = Marshal.SizeOf(typeof(T));
            mangagedArray = new T[length];

            for (int i = 0; i < length; i++) {
                IntPtr ins = new IntPtr(unmanagedArray.ToInt64() + i * size);
                mangagedArray[i] = Marshal.PtrToStructure<T>(ins);
            }
        }

        /// <summary>
        /// This function should be called at the beginning of your program
        /// </summary>
        public static void confd_init() {

         
            var fh = libconfd.fopen(config.COONFDNET_LOGFILE, "w"); //TODO Error Handling
            
            Console.WriteLine(fh);
            libconfd.confd_init_vsn_sz("confdsharp", fh, (int)config.CONFD_DEBUG_LEVEL, constants.CONFD_LIB_API_VSN, constants.MAXDEPTH, constants.MAXKEYLEN);
        }

        public static int confd_version ;
        public static byte confd_maxdepth ;
        public static byte confd_maxkeylen;

        /// <summary>
        /// Check if an error happened in confd, and returns an exception if so.
        /// </summary>
        /// <param name="code">Return code from a confd function</param>
        /// <param name="msg">Message to show if there was an error</param>
        /// <param name="checkLastError">Check system last error as well ?</param>
        /// <returns></returns>
        public static bool confd_check_error(int code, string msg, bool checkLastError=true) {
            int marshalerr = 0;
            if (checkLastError) {
                marshalerr = Marshal.GetLastWin32Error();
            }
            if(code != 0|| marshalerr != 0) {
                throw new Exception("Code: " + code.ToString() + " Marshal Error: " + marshalerr.ToString() + " - " + msg + " - " + libconfd.confd_lasterr_string()) ;
            }
                    
     
            return false;
        }


        /*
          #define get_int16(s) ((((unsigned char*)  (s))[0] << 8) | \
                      (((unsigned char*)  (s))[1]))


        #define put_int16(i, s) {((unsigned char*)(s))[0] = ((i) >> 8) & 0xff; \
                                ((unsigned char*)(s))[1] = (i)         & 0xff;}



        #define get_int32(s) ((((unsigned char*) (s))[0] << 24) | \
                              (((unsigned char*) (s))[1] << 16) | \
                              (((unsigned char*) (s))[2] << 8)  | \
                              (((unsigned char*) (s))[3]))

        #define put_int32(i, s) {((char*)(s))[0] = (char)((i) >> 24) & 0xff; \
                                 ((char*)(s))[1] = (char)((i) >> 16) & 0xff; \
                                 ((char*)(s))[2] = (char)((i) >> 8)  & 0xff; \
                                 ((char*)(s))[3] = (char)((i)        & 0xff);}
       */

        /// <summary>
        /// Write int16 data to buffer
        /// </summary>
        /// <param name="data">data to write</param>
        /// <param name="buf">buffer</param>
        /// <param name="index">Start index</param>
        public static void put_int16(short data, ref byte[] buf, int index) {

            var arr = BitConverter.GetBytes(data);
            Array.Reverse(arr);
            arr.CopyTo(buf, index);
        }

        /// <summary>
        /// Write int32 data to buffer
        /// </summary>
        /// <param name="data">data to write</param>
        /// <param name="buf">buffer</param>
        /// <param name="index">Start index</param>
        public static void put_int32(int data, ref byte[] buf, int index) {
            //BitConverter.GetBytes(data).CopyTo(buf, index);
            var arr = BitConverter.GetBytes(data);
            Array.Reverse(arr);
            arr.CopyTo(buf, index);
        }

        /// <summary>
        /// Get int32 data from buffer
        /// </summary>
        /// <param name="buf">buffer</param>
        /// <param name="index">Start index</param>
        /// <returns>Int32 data</returns>
        public static int get_int32( byte[] buf, int index) {
            byte[] arr = new byte[4];
            Array.Copy(buf, index, arr, 0, 4);
            Array.Reverse(arr);
            return BitConverter.ToInt32(arr, 0);
        }

        /// <summary>
        /// Get int16 data from buffer
        /// </summary>
        /// <param name="buf">buffer</param>
        /// <param name="index">Start index</param>
        /// <returns>Int16 data</returns>
        public static short get_int16( byte[] buf, int index) {
            byte[] arr = new byte[2];
            Array.Copy(buf, index, arr, 0, 2);
            Array.Reverse(arr);
            return BitConverter.ToInt16(arr, 0);
        }

        /// <summary>
        /// Makes client ID to use with confd functions
        /// </summary>
        /// <returns></returns>
        public static string make_client_id() {
            return "<stdin>:1";
        }

        /// <summary>
        /// Connect to confd server
        /// </summary>
        /// <param name="sock">Socket</param>
        /// <param name="ipe">IP Address</param>
        /// <param name="id">Client ID, should be constants.CLIENT_MAAPI</param>
        public static void confd_do_connect(Socket sock, IPEndPoint ipe, byte id) {
            byte[] buf = new byte[9];
            byte[] reply = new byte[10]; //Maybe change to 9 ?
            bool do_access = config.confd_ipc_access_secret != String.Empty;
            //int one = 1;

            if (libconfd.confd_check_init() != (int)confd_return_codes.CONFD_OK) {
                throw new ConfdException("ConfD Not initialized", (int)confd_return_codes.CONFD_ERR);
            }
            libconfd.clr_confd_err();
            sock.Connect(ipe); //TODO: Try/Catch Failed to connect to ConfD:




            buf[0] = id;
            if (do_access) {
                buf[0] |= (byte)constants.IPC_PROTO_WANT_CHALLENGE;
            }
            put_int16(constants.CONFD_LIB_PROTO_VSN, ref buf, 1);
            put_int32(constants.CONFD_LIB_VSN, ref buf, 3);
            buf[7] = constants.MAXDEPTH;
            buf[8] = constants.MAXKEYLEN;
            sock.Send(buf);
            if (do_access) {
                throw new NotImplementedException("Auth not supported at this time");
            }

            /*if ((ret = confd_write(sock, buf, 9)) != CONFD_OK)
                return ret;
            if (do_access) {
                switch (confd_ipc_access_check(sock, confd_ipc_access_secret)) {
                case 1:  // success 
                    break;
                case 0:
                    return ret_err(CONFD_ERR_PROTOUSAGE, "Access denied");
                default:
                    return ret_err(CONFD_ERR_PROTOUSAGE, "Access check failed");
                }
            } */

            sock.Receive(reply, 9, 0);
            switch (reply[0]) {
                case (byte)constants.IPC_PROTO_OK:
                    confd_version = get_int32(reply, 3);
                    confd_maxdepth = reply[7];
                    confd_maxkeylen = reply[8];
                    return;
                case (byte)constants.IPC_PROTO_BAD_VSN:
                    string errmessage = string.Format("Library protocol version ({0}) is not compatible with ConfD protocol version ({1})",
                        constants.CONFD_LIB_PROTO_VSN, get_int16(reply,1));
                    throw new ConfdException(errmessage, constants.CONFD_ERR_PROTOUSAGE);
                case (byte)constants.IPC_PROTO_BAD_SIZES:
                    confd_maxdepth = reply[7];
                    confd_maxkeylen = reply[8];

                    string errmessage2 = string.Format("Library MAXDEPTH / MAXKEYLEN {0} / {1} for confd_hkeypath_t are too small, ConfD needs {2} / {3}",
                       constants.MAXDEPTH, constants.MAXKEYLEN, confd_maxdepth, confd_maxkeylen);
                    throw new ConfdException(errmessage2, constants.CONFD_ERR_PROTOUSAGE);
                    
                default:
                    throw new ConfdException("Invalid Reply: "+ reply[0].ToString(), constants.CONFD_ERR_PROTOUSAGE);
            }

           
        }
    }
}
