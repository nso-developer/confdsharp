﻿using System;
using System.Runtime.InteropServices;

namespace confdsharp
{
    public static class constants
    {
        /*
         * >>> import _ncs
        >>> _ncs.LIB_API_VSN
        101123328
        >>> hex(_ncs.LIB_API_VSN)
        '0x6070500'
        >>> _ncs.LIB_API_VSN_STR
        '06070500'
        >>> _ncs.LIB_PROTO_VSN
        67
        >>> _ncs.LIB_PROTO_VSN_STR
        '67'
        >>> _ncs.LIB_VSN
        101123328
        >>> _ncs.LIB_VSN_STR
        '06070500'
        >>>
        */

        //NSO 4.7.5
        /*
        public const int CONFD_LIB_API_VSN = 0x06070500;
        public const int CONFD_LIB_VSN = 0x06070500;
        public const string CONFD_LIB_VSN_STR = "06070000";
        public const string CONFD_LIB_API_VSN_STR = "06070000";
        public const int CONFD_LIB_PROTO_VSN = 67;
        public const string CONFD_LIB_PROTO_VSN_STR = "67";
        */
        //NSO 5.4.1
        public const int CONFD_LIB_API_VSN = 0x7040000;
        public const int CONFD_LIB_VSN = 0x7040000;
        public const string CONFD_LIB_VSN_STR = "07040100";
        public const string CONFD_LIB_API_VSN_STR = "07040100";
        public const int CONFD_LIB_PROTO_VSN = 74;
        public const string CONFD_LIB_PROTO_VSN_STR = "74";


        public const int MAXDEPTH = 60;
        public const int MAXKEYLEN = 18;
        public const int CONFD_VALUE_TYPE_SIZE = 40; 
        /// <summary>
        /// IPv4 protocol family.
        /// </summary>
        public const byte AF_INET = 2;
        public const byte SOCK_STREAM = 1;

        public const int CLIENT_CAPI = 3;
        public const int CLIENT_CDB = 5;
        public const int CLIENT_MAAPI = 7;
        public const int CLIENT_STREAM = 8;
        public const int CLIENT_EVENT_MGR = 11;
        public const int CLIENT_HA = 17;

        public const int IPC_PROTO_OK = 0;
        public const int IPC_PROTO_BAD_VSN = 1;
        public const int IPC_PROTO_BAD_SIZES = 2;
        public const int IPC_PROTO_WANT_CHALLENGE = (1 << 7); // ORed with CLIENT_XXX


        public const int CONFD_TRANS_REPLY_OK = 101;
        public const int CONFD_TRANS_REPLY_ERROR = 102;
        public const int CONFD_DATA_REPLY_VALUE = 103;
        public const int CONFD_DATA_REPLY_OK = 104;
        public const int CONFD_DATA_REPLY_ERROR = 105;


        public const int CONFD_ERR_NOEXISTS = 1;
        public const int CONFD_ERR_ALREADY_EXISTS = 2;
        public const int CONFD_ERR_ACCESS_DENIED = 3;
        public const int CONFD_ERR_NOT_WRITABLE = 4;
        public const int CONFD_ERR_BADTYPE = 5;
        public const int CONFD_ERR_NOTCREATABLE = 6;
        public const int CONFD_ERR_NOTDELETABLE = 7;
        public const int CONFD_ERR_BADPATH = 8;
        public const int CONFD_ERR_NOSTACK = 9;
        public const int CONFD_ERR_LOCKED = 10;
        public const int CONFD_ERR_INUSE = 11;

        /* error codes from apply and validate */
        public const int CONFD_ERR_NOTSET = 12;
        public const int CONFD_ERR_NON_UNIQUE = 13;
        public const int CONFD_ERR_BAD_KEYREF = 14;
        public const int CONFD_ERR_TOO_FEW_ELEMS = 15;
        public const int CONFD_ERR_TOO_MANY_ELEMS = 16;
        public const int CONFD_ERR_BADSTATE = 17;

        public const int CONFD_ERR_INTERNAL = 18;
        public const int CONFD_ERR_EXTERNAL = 19;
        public const int CONFD_ERR_MALLOC = 20;
        public const int CONFD_ERR_PROTOUSAGE = 21;
        public const int CONFD_ERR_NOSESSION = 22;
        public const int CONFD_ERR_TOOMANYTRANS = 23;
        public const int CONFD_ERR_OS = 24;

        /* ha related errors */
        public const int CONFD_ERR_HA_CONNECT = 25;
        public const int CONFD_ERR_HA_CLOSED = 26;
        public const int CONFD_ERR_HA_BADFXS = 27;
        public const int CONFD_ERR_HA_BADTOKEN = 28;
        public const int CONFD_ERR_HA_BADNAME = 29;
        public const int CONFD_ERR_HA_BIND = 30;
        public const int CONFD_ERR_HA_NOTICK = 31;

        /* maapi_validate()=*/
        public const int CONFD_ERR_VALIDATION_WARNING = 32;
        public const int CONFD_ERR_SUBAGENT_DOWN = 33;
        public const int CONFD_ERR_LIB_NOT_INITIALIZED = 34;
        public const int CONFD_ERR_TOO_MANY_SESSIONS = 35;
        public const int CONFD_ERR_BAD_CONFIG = 36;
        /* maapi for extended errors */
        public const int CONFD_ERR_RESOURCE_DENIED = 37;
        public const int CONFD_ERR_INCONSISTENT_VALUE = 38;
        public const int CONFD_ERR_APPLICATION_INTERNAL = 39;
        /* more error codes from apply and validate */
        public const int CONFD_ERR_UNSET_CHOICE = 40;
        public const int CONFD_ERR_MUST_FAILED = 41;
        public const int CONFD_ERR_MISSING_INSTANCE = 42;
        public const int CONFD_ERR_INVALID_INSTANCE = 43;

        /* (e.g.) maapi_get_attrs()/maapi_set_attr() */
        public const int CONFD_ERR_UNAVAILABLE = 44;

        /* used when API function returns CONFD_EOF */
        public const int CONFD_ERR_EOF = 45;

        /* maapi_move* */
        public const int CONFD_ERR_NOTMOVABLE = 46;

        /* in-service upgrade */
        public const int CONFD_ERR_HA_WITH_UPGRADE = 47;
        public const int CONFD_ERR_TIMEOUT = 48;
        public const int CONFD_ERR_ABORTED = 49;

        /* xpath compilation/evaluation */
        public const int CONFD_ERR_XPATH = 50;

        /* not implemented CDB/MAAPI op */
        public const int CONFD_ERR_NOT_IMPLEMENTED = 51;

        /* HA incompatible version */
        public const int CONFD_ERR_HA_BADVSN = 52;

        /* error codes for configuration policies from apply and validate */
        public const int CONFD_ERR_POLICY_FAILED = 53;
        public const int CONFD_ERR_POLICY_COMPILATION_FAILED = 54;
        public const int CONFD_ERR_POLICY_EVALUATION_FAILED = 55;

        /* NCS failed to connect to a device */
        public const int NCS_ERR_CONNECTION_REFUSED = 56;

        /* error code when maapi_start_phase() fails */
        public const int CONFD_ERR_START_FAILED = 57;

        /* more maapi for extended error */
        public const int CONFD_ERR_DATA_MISSING = 58;

        /* error code for cli command */
        public const int CONFD_ERR_CLI_CMD = 59;

        /* error code for disallowed operations in upgrade phase */
        public const int CONFD_ERR_UPGRADE_IN_PROGRESS = 60;

        /* invalid transaction handle passed to a maapi function */
        public const int CONFD_ERR_NOTRANS = 61;

        /* NCS specific errors */
        public const int NCS_ERR_SERVICE_CONFLICT = 62;
        public const int NCS_ERR_CONNECTION_TIMEOUT = 63;
        public const int NCS_ERR_CONNECTION_CLOSED = 64;
        public const int NCS_ERR_DEVICE = 65;
        public const int NCS_ERR_TEMPLATE = 66;

        /* path is ambiguous due to traversing a mount point */
        public const int CONFD_ERR_NO_MOUNT_ID = 67;



        /* Used to detect node type with flags of cs_node_info struct */
        public const int CS_NODE_IS_LIST = (1 << 0);
        public const int CS_NODE_IS_WRITE = (1 << 1);
        public const int CS_NODE_IS_CDB = (1 << 2);
        public const int CS_NODE_IS_ACTION = (1 << 3);
        public const int CS_NODE_IS_PARAM = (1 << 4);
        public const int CS_NODE_IS_RESULT = (1 << 5);
        public const int CS_NODE_IS_NOTIF = (1 << 6);
        public const int CS_NODE_IS_CASE = (1 << 7);
        public const int CS_NODE_IS_CONTAINER = (1 << 8);
        public const int CS_NODE_HAS_WHEN = (1 << 9);
        public const int CS_NODE_HAS_DISPLAY_WHEN = (1 << 10);
        public const int CS_NODE_HAS_META_DATA = (1 << 11);
        public const int CS_NODE_IS_WRITE_ALL = (1 << 12);
        public const int CS_NODE_IS_LEAF_LIST = (1 << 13);
        public const int CS_NODE_IS_LEAFREF = (1 << 14);
        public const int CS_NODE_HAS_MOUNT_POINT = (1 << 15);
        public const int CS_NODE_IS_STRING_AS_BINARY = (1 << 16);
        public const int CS_NODE_IS_DYN = CS_NODE_IS_LIST; /* backwards compat */

        /* cmp values in confd_cs_node_info */
        public const int CS_NODE_CMP_NORMAL = 0;
        public const int CS_NODE_CMP_SNMP = 1;
        public const int CS_NODE_CMP_SNMP_IMPLIED = 2;
        public const int CS_NODE_CMP_USER = 3;
        public const int CS_NODE_CMP_UNSORTED = 4;
    }
}
