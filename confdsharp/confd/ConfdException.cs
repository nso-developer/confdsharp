﻿using System;
namespace confdsharp.confd
{
    public class ConfdException : Exception
    {
        public int ConfdErrorCode = 0;

        public ConfdException() : base() {
        }

        public ConfdException(string message) : base(message) {
        }

        public ConfdException(string message, int errorcode) : base(message) {
            this.ConfdErrorCode = errorcode;
        }
    }
}
