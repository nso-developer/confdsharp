﻿using System;
using System.Net.Sockets;
using System.Runtime.InteropServices;

namespace confdsharp
{
    public class libconfd
    {
        /*
        //https://stackoverflow.com/questions/53606321/calling-linux-socket-and-epoll-functions-in-c-sharp
        [DllImport("libc", EntryPoint = "socket", SetLastError = true)]
        public static extern int socket(int domain, int type, int protocol);

        // https://github.com/mono/mono/blob/master/mcs/class/Mono.Posix/Mono.Unix.Native/Syscall.cs


        [DllImport("libc", EntryPoint = "close", SetLastError = true)]
        public static extern int close(int handle);

        [DllImport("libc", EntryPoint = "connect", SetLastError = true)]
        public static extern int connect(int socket, sockaddr_in addr, int socklen_t);
        */

        //TODO: Avoid return string. See: https://stackoverflow.com/questions/61122557/dotnet-malloc-double-free-of-object-when-calling-dllimport-function/61122677#61122677
        [DllImport("libc", CharSet = CharSet.Ansi)]
        public static extern IntPtr fopen(String filename, String mode);

        [DllImport("libc", CharSet = CharSet.Ansi)]
        public static extern Int32 fclose(IntPtr file);

        [DllImport("libconfd.so", SetLastError = true)]
        public static extern void confd_init_vsn_sz(string name,  IntPtr file, int debug, int vsn, int maxdepth, int maxkeylen);

        [DllImport("libconfd.so", SetLastError = true)]
        public static extern int maapi_start_trans_flags2(int sock, int dbname,  int readwrite, int usid, int flags,
           string vendor,  string product,
           string version, string client_id);


        [DllImport("libconfd.so", SetLastError = true)]
        public static extern int maapi_apply_trans_flags(int sock, int tid, int keepopen, int flags);




        [DllImport("libconfd.so", SetLastError = true)]
        public static extern int maapi_start_user_session3(int sock,  string username,  string context,
            string[] groups, int numgroups,
            ref confd_ip src_addr, int src_port, int prot,  string vendor, string product,
            string version, string client_id); //confd_proto

        [DllImport("libconfd.so", SetLastError = true)]
        public static extern int maapi_load_schemas(int sock);

        [DllImport("libconfd.so", SetLastError = true)]
        public static extern int maapi_get_schema_file_path(int sock, ref string buf);

        [DllImport("libconfd.so", SetLastError = true)]
        public static extern int confd_mmap_schemas(string filename);

        [DllImport("libconfd.so", SetLastError = true)]
        public static extern int maapi_get_elem(int sock, int thandle, IntPtr v, string fmt);


        //Another note which might effect users is that if the type we are writing is any of the encrypt or hash types, the maapi_set_elem2() 
        //will perform the asymmetric conversion of values whereas the maapi_set_elem() will not. See confd_types(3),
        //the types tailf:md5-digest-string, tailf:des3-cbc-encrypted-string, and tailf:aes-cfb-128-encrypted-string.
        [DllImport("libconfd.so", SetLastError = true)] 
        public static extern int maapi_set_elem2(int sock, int thandle, IntPtr v /*ref confd_value_t v*/, string fmt);
        [DllImport("libconfd.so", SetLastError = true)]
        public static extern int maapi_set_elem(int sock, int thandle, IntPtr v /*ref confd_value_t v*/, string fmt);

        [DllImport("libconfd.so", SetLastError = true)]
        public static extern int maapi_connect(int socket, sockaddr_in addr, int srv_sz);
        [DllImport("libconfd.so", SetLastError = true)]
        public static extern IntPtr confd_lasterr();

        public static string confd_lasterr_string() {
            return Marshal.PtrToStringAnsi(confd_lasterr());
        }
        [DllImport("libconfd.so", SetLastError = true)]
        public static extern int confd_check_init();
        [DllImport("libconfd.so", SetLastError = true)]
        public static extern int clr_confd_err();

        #region namespaces
        [DllImport("libconfd.so", SetLastError = true)]
        public static extern int confd_get_nslist(ref IntPtr ptr); //confd_nsinfo

        [DllImport("libconfd.so", SetLastError = true)]
        public static extern IntPtr confd_ns2prefix(UInt32 ns);

        public static  string confd_ns2prefix_w(UInt32 ns) {
            return Marshal.PtrToStringAnsi(confd_ns2prefix(ns));
        }

        [DllImport("libconfd.so", SetLastError = true)]
        private static extern IntPtr confd_hash2str(UInt32 hash);

        public static string confd_hash2str_w(UInt32 hash) {
            return Marshal.PtrToStringAnsi(confd_hash2str(hash));
        }

        [DllImport("libconfd.so", SetLastError = true)]
        public static extern UInt32 confd_str2hash(string str);

        [DllImport("libconfd.so", SetLastError = true)]
        public static extern  IntPtr confd_find_cs_root(int ns); //confd_cs_node

        [DllImport("libconfd.so", SetLastError = true)]
        public static extern IntPtr confd_find_cs_node(ref confd_hkeypath_t hkeypath, int len); //confd_cs_node

        [DllImport("libconfd.so", SetLastError = true)]
        public static extern IntPtr confd_find_cs_node_child( ref confd_cs_node parent,  xml_tag xmltag); //confd_cs_node

        [DllImport("libconfd.so", SetLastError = true)]
        public static extern  IntPtr confd_cs_node_cd( ref confd_cs_node start, string fmt); //confd_cs_node

        [DllImport("libconfd.so", SetLastError = true)]
        public static extern confd_vtype confd_get_base_type(ref confd_cs_node node);

        [DllImport("libconfd.so", SetLastError = true)]
        public static extern int confd_max_object_size(ref confd_cs_node obj);

        [DllImport("libconfd.so", SetLastError = true)]
        public static extern  IntPtr confd_next_object_node( ref confd_cs_node obj, ref confd_cs_node cur,  ref confd_value_t value); //confd_cs_node

        [DllImport("libconfd.so", SetLastError = true)]
        public static extern  IntPtr confd_find_ns_type(UInt32 nshash,  string name); // confd_type
        #endregion

    }

    //https://docs.microsoft.com/en-us/dotnet/framework/interop/passing-structures?redirectedfrom=MSDN
}


//https://github.com/dotnet/corefx/blob/79d44f202e4b6d000ca6c6885a24549a55db38f1/src/System.Console/src/Interop/Interop.manual.Unix.cs
// nm -Ca libconfd.so | grep confd_init




/*
 *     if ((subsock = socket(PF_INET, SOCK_STREAM, 0)) < 0 )
    confd_fatal("Failed to open socket\n");

if (cdb_connect(subsock, CDB_SUBSCRIPTION_SOCKET,
                (struct sockaddr*)&addr,
                sizeof (struct sockaddr_in)) < 0)
    confd_fatal("Failed to confd_connect() to confd \n");

if ((status =
     cdb_subscribe(subsock, 3, servers__ns, &spoint,"/servers"))
    != CONFD_OK) {
    fprintf(stderr, "Terminate: subscribe %d\n", status);
    exit(1);
}
if (cdb_subscribe_done(subsock) != CONFD_OK)
    confd_fatal("cdb_subscribe_done() failed");

if ((status = read_conf(&addr)) != CONFD_OK) {
    fprintf(stderr, "Terminate: read_conf %d\n", status);
    exit(1);
}

}
https://stackoverflow.com/questions/8504992/converting-c-to-c-sharp
    */
