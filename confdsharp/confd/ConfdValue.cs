﻿using System;
using System.Net;
using System.Runtime.InteropServices;

namespace confdsharp.confd
{
    public class ConfdValue
    {
        private IntPtr _value;
        public dynamic Value {
            get {
                return this.getValue();
            } set {
                this.setValue(value);
            }
        }

        public ConfdValue(dynamic value) {
            this.setValue(value);
           
        }

        public ConfdValue() {
        }


        /*
[StructLayout(LayoutKind.Explicit)] //https://stackoverflow.com/questions/51618837/c-sharp-marshal-unmanaged-pointer-return-type
public struct confd_value_t //https://stackoverflow.com/questions/1498931/marshalling-array-of-strings-to-char-in-c-sharp
{
   [FieldOffset(0)]
   public confd_vtype vtype;  // as defined above 

//struct xml_tag xmltag;
[FieldOffset(4)]
   uint symbol;
   //confd_buf_t buf;
   //confd_buf_const_t c_buf;
   [FieldOffset(4)]
   string s; //char* = string ?

   [FieldOffset(4)]
   string c_s; //const char*

   [FieldOffset(4)]
   byte i8;

   [FieldOffset(4)]
   short i16;

   [FieldOffset(4)]
   int i32;
   //int64_t i64;
   //u_int8_t u8;
   //u_int16_t u16;
   //u_int32_t u32;
   //u_int64_t u64;
   //double d;

   //[FieldOffset(4)]
   //struct in_addr ip;
   //[FieldOffset(4)]
   //struct in6_addr ip6;
   //int boolean;
   //struct confd_qname qname;
   //struct confd_datetime datetime;
   //struct confd_date date;
   //struct confd_time time;
   //struct confd_duration duration;
   //int32_t enumvalue;
   //u_int32_t b32;
   //u_int64_t b64;
   //struct confd_list list;
   //struct confd_hkeypath *hkp;
   //struct confd_vptr ptr;
   //struct confd_snmp_oid *oidp;
   //struct confd_ipv4_prefix ipv4prefix;
   //struct confd_ipv6_prefix ipv6prefix;
   //struct confd_decimal64 d64;
   //struct confd_identityref idref;
   //struct confd_dotted_quad dquad;
   //DO not do this one //u_int32_t enumhash;     // backwards compat 


} */


        private void setValue ( dynamic value) {
            _value = value;
            if (value is IPAddress) {
                if (((IPAddress)value).AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork) {
                    setIPV4Value(value);
                } else {
                    setIPV6Value(value);
                }

                
            } else if (value is sbyte) {
                Marshal.WriteInt64(_value, (int)confd_vtype.C_INT8);
                Marshal.WriteByte(_value+8, (byte)value);
            } else if (value is int || value is Int32) {
                Marshal.WriteInt64(_value, (int)confd_vtype.C_INT32);
                Marshal.WriteInt32(_value + 8, (int)value);
            } else if (value is short) {
                Marshal.WriteInt64(_value, (int)confd_vtype.C_INT16);
                Marshal.WriteInt16(_value + 8, (short)value);


            } else if (value is long) {
                Marshal.WriteInt64(_value, (int)confd_vtype.C_INT64);
                Marshal.WriteInt64(_value + 8, (long)value);
               

            } else if (value is byte) {
                Marshal.WriteInt64(_value, (int)confd_vtype.C_UINT8);
                Marshal.WriteByte(_value + 8, (byte)value);
            } else if (value is uint || value is UInt32) {
                Marshal.WriteInt64(_value, (int)confd_vtype.C_UINT32);
                Marshal.WriteInt32(_value + 8, (Int32)value);

            } else if (value is ushort) {
                Marshal.WriteInt64(_value, (int)confd_vtype.C_UINT16);
                Marshal.WriteInt16(_value + 8, (short)value);


            } else if (value is ulong) {
                Marshal.WriteInt64(_value, (int)confd_vtype.C_UINT64);
                Marshal.WriteInt64(_value + 8, (long)value);

            } else if (value is string) {
                // this.confd_value.string_value = value;
                Marshal.WriteInt64(_value, (int)confd_vtype.C_STR); //C_BUF? 
                IntPtr stptr = Marshal.StringToHGlobalAnsi(value);
                Marshal.WriteIntPtr(_value + 8, stptr);
                

            } else if (value is IntPtr) {
                this._value = value;
            } else {
                throw new NotImplementedException();
            }

        }
        private dynamic getValue() {
            /*
             * /Users/xxx/NSO/4.7.5/src/ncs/pyapi/src/types.c confdValue_init:596
             * check confd_value_t *eterm_to_val(const ETERM *term, confd_value_t *v) confd_internal.c:170
             */
            confd_vtype vtype = (confd_vtype)Marshal.ReadInt64(_value);
            switch (vtype) { 
                case confd_vtype.C_IPV4:
                    return new IPAddress(Marshal.ReadInt32(_value + 8));
                case confd_vtype.C_IPV6:
                    return getIPV6Value();
                case confd_vtype.C_UINT8:
                    return Marshal.ReadByte(_value + 8);
                case confd_vtype.C_UINT16:
                    return (ushort)Marshal.ReadInt16(_value + 8);
                case confd_vtype.C_UINT32:
                    return (UInt32)Marshal.ReadInt32(_value + 8);
                case confd_vtype.C_UINT64:
                    return (ulong)Marshal.ReadInt64(_value + 8);
                case confd_vtype.C_INT8:
                    return (sbyte)Marshal.ReadByte(_value + 8);
                case confd_vtype.C_INT16:
                    return Marshal.ReadInt16(_value + 8);
                case confd_vtype.C_INT32:
                    return Marshal.ReadInt32(_value + 8);
                case confd_vtype.C_INT64:
                    return Marshal.ReadInt64(_value + 8);
                case confd_vtype.C_BUF:
                    var buf = Marshal.PtrToStructure<confd_buf>(_value + 8);
                    return buf.data;
                case confd_vtype.C_STR:
                    IntPtr sptr = Marshal.ReadIntPtr(_value + 8);
                    return Marshal.PtrToStringAnsi(sptr);
                default:
                    throw new NotImplementedException();
            }
        }




        private IPAddress getIPV6Value() {
            throw new NotImplementedException();
            //var ip = new IPAddress(confd_value.ipv6.address);//TODO Maybe we need reverse here
            //return ip;
        }

        private void setIPV4Value ( IPAddress ipv4) {
         


        }

        private void setIPV6Value(IPAddress ipv6) {
           

        }
    }
}
