﻿using System;
using System.Net;
using System.Runtime.InteropServices;

namespace confdsharp.Maapi
{
    public class Session
    {
        public Maapi maapi;
        private string src_ip;
        private string user;
        private string context;
        string client_id;

        /// <summary>
        /// Start a Session
        /// </summary>
        /// <param name="maapi"></param>
        /// <param name="user"></param>
        /// <param name="context"></param>
        /// <param name="groups"></param>
        /// <param name="src_ip"></param>
        /// <param name="src_port"></param>
        /// <param name="proto"></param>
        /// <param name="vendor"></param>
        /// <param name="product"></param>
        /// <param name="version"></param>
        /// <param name="client_id"></param>
        public Session(Maapi maapi, string user, string context, string[] groups = null, string src_ip = "127.0.0.1",
                 int src_port = 0, confd_proto proto = confd_proto.CONFD_PROTO_TCP, string vendor = null, string product =  null,
                 string version = null, string client_id = null) {

            groups = groups ?? new string[0];
            this.user = user;
            this.context = context;
            this.maapi = maapi;
            this.src_ip = src_ip;
            var ip_struct = this.get_src_ip();

            
            this.client_id = client_id ?? helpers.make_client_id();
            
            
            int r = libconfd.maapi_start_user_session3((int)maapi.msock.Handle, user, context, groups, groups.Length,  ref ip_struct,0, (int) proto,
                   vendor, product, version, this.client_id);

          

            helpers.confd_check_error(r, "Error starting Session");
            
        }


        /// <summary>
        /// Format source IP in proper format
        /// </summary>
        /// <returns></returns>
        private confd_ip get_src_ip() {
            IPAddress addr = IPAddress.Parse(this.src_ip);
            confd_ip ret = new confd_ip();
            ret.af = constants.AF_INET;
            Byte[] addrbytes = addr.GetAddressBytes();
            //Array.Reverse(addrbytes);
            ret.ip = new byte[16];
            Array.Clear(ret.ip, 0, ret.ip.Length);
            addrbytes.CopyTo(ret.ip, 0);
            return ret;
        }

    }
}
