﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using confdsharp.Maagic;

namespace confdsharp.Maapi
{
    public class Maapi
    {
        public Socket msock;
        string ip;
        int port;
        string path;
        private int th = 0;
        public IPEndPoint ipEndpoint;
        string client_id;
        private bool schemas_loaded = false;
        /// <summary>
        /// Maapi Constructor
        /// </summary>
        /// <param name="ip">IP Address of confd server</param>
        /// <param name="port">Port of confd server</param>
        /// <param name="path">Not used atm</param>
        /// <param name="load_schemas">Automatically call load_schemas function</param>
        /// <param name="msock">Allows to reuse an existing socket</param>
        public Maapi(string ip = "127.0.0.1", int port = 4569, string path = "", bool load_schemas = true, Socket msock = null) {
            if (msock != null) {
                this.msock = msock;
            } else {
                this.msock = this.connect(ip, port, path);
            }
            this.port = port;
            this.ip = ip;
            this.path = path;
            if (load_schemas) {
                this.load_schemas();
            }
        }

        /// <summary>
        /// Function to load the data schemas in memmory
        /// </summary>
        /// <param name="use_maapi_socket">Load schemas through Socket if set to true</param>
        public void load_schemas(bool use_maapi_socket = true) {
            
            if (schemas_loaded) { return; }
            
            if (use_maapi_socket) {
                int r = libconfd.maapi_load_schemas((int)this.msock.Handle);
                helpers.confd_check_error(r, "Error loading schemas", false);//TODO: Not sure why get_last_error gives 22 here...
                this.schemas_loaded = true;
            } else {
                string path="";
                int r = libconfd.maapi_get_schema_file_path((int)this.msock.Handle, ref path);
                helpers.confd_check_error(r, "Error Loading Schemas: Can't get path");
                r = libconfd.confd_mmap_schemas(path);
                helpers.confd_check_error(r, "Error Loading Schemas: mmap failed");
                this.schemas_loaded = true;
            }
            
        }

        ~Maapi() {
            if (this.msock.Connected) {
                this.msock.Close();
            }

        }

        /// <summary>
        /// Connect to ConfD server
        /// </summary>
        /// <param name="ip">Confd server IP</param>
        /// <param name="port">Confd server Port</param>
        /// <param name="path">Not used atm</param>
        /// <returns>Connected socket</returns>
        public Socket connect(string ip = "127.0.0.1", int port = 4569, string path = "") {

            this.ipEndpoint = new IPEndPoint(IPAddress.Parse(ip), port);

            this.msock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); //TODO Support for IPv6 etc...


            if ( String.IsNullOrEmpty(path)) { //Path is used for unix socket, we don't do anything here

            } else {

            }
          

            helpers.confd_do_connect(this.msock, this.ipEndpoint, constants.CLIENT_MAAPI);
            return this.msock;
        }

        /// <summary>
        /// Apply transaction with flags
        /// </summary>
        /// <param name="tid">Transaction ID</param>
        /// <param name="keepopen">Keep transaction opened</param>
        /// <param name="flags">ConfD Transaction Flags</param>
        public void maapi_apply_trans_flags(int tid, int keepopen, int flags) {
            int r = libconfd.maapi_apply_trans_flags((int)this.msock.Handle, tid, keepopen, flags);
            helpers.confd_check_error(r, "Error in maapi_apply_trans_flags");
        }

        /// <summary>
        /// Apply currrent transaction with flags
        /// </summary>
        /// <param name="keepopen">Keep transaction opened</param>
        /// <param name="flags">ConfD Transaction Flags</param>
        public void maapi_apply_trans_flags(int keepopen, int flags) {
            int r = libconfd.maapi_apply_trans_flags((int)this.msock.Handle, this.th, keepopen, flags);
            helpers.confd_check_error(r, "Error in maapi_apply_trans_flags");
        }

        /// <summary>
        /// Apply Current transaction
        /// </summary>
        /// <param name="keepopen">Keep transaction opened</param>
        public void maapi_apply_trans(int keepopen=0) {
            int r = libconfd.maapi_apply_trans_flags((int)this.msock.Handle, this.th, keepopen, 0);
            helpers.confd_check_error(r, "Error in maapi_apply_trans");
        }


        /// <summary>
        /// Start a new transaction
        /// </summary>
        /// <param name="rw">Read/Write mode</param>
        /// <param name="db">Database</param>
        /// <param name="usid">User ID</param>
        /// <param name="flags">Transaction Flags</param>
        /// <param name="vendor">Vendor</param>
        /// <param name="product">Product</param>
        /// <param name="version">Version</param>
        /// <param name="client_id">Client_id</param>
        /// <returns></returns>
        public Transaction start_trans(confd_trans_mode rw = confd_trans_mode.CONFD_READ, confd_dbname db = confd_dbname.CONFD_RUNNING, int usid = 0, int flags = 0,
                    string vendor = null, string product = null, string version = null, string client_id = null) {

            this.client_id = client_id ?? helpers.make_client_id();
            this.th = libconfd.maapi_start_trans_flags2((int)this.msock.Handle, (int)db, (int)rw, usid, flags,
                                          vendor, product, version, this.client_id);

            return new Transaction(this, this.th, rw, db,vendor,product,version, this.client_id);
        }

        /// <summary>
        /// Start a new read transaction
        /// </summary>
        /// <param name="db">Database</param>
        /// <param name="usid">User ID</param>
        /// <param name="flags">Transaction Flags</param>
        /// <param name="vendor">Vendor</param>
        /// <param name="product">Product</param>
        /// <param name="version">Version</param>
        /// <param name="client_id">Client_id</param>
        /// <returns></returns>
        public Transaction start_read_trans(confd_dbname db = confd_dbname.CONFD_RUNNING, int usid = 0, int flags = 0,
                    string vendor = null,  string product = null, string version = null, string client_id = null) {
            return this.start_trans(confd_trans_mode.CONFD_READ, db, usid, flags, vendor, product, version, client_id);
        }

        /// <summary>
        /// Start a new write transaction
        /// </summary>
        /// <param name="db">Database</param>
        /// <param name="usid">User ID</param>
        /// <param name="flags">Transaction Flags</param>
        /// <param name="vendor">Vendor</param>
        /// <param name="product">Product</param>
        /// <param name="version">Version</param>
        /// <param name="client_id">Client_id</param>
        /// <returns></returns>
        public Transaction start_write_trans(confd_dbname db = confd_dbname.CONFD_RUNNING, int usid = 0, int flags = 0,
            string vendor = null, string product = null, string version = null, string client_id = null) {
            return this.start_trans(confd_trans_mode.CONFD_READ_WRITE, db, usid, flags, vendor, product, version, client_id);
        }

        /// <summary>
        /// Returns cdb root node
        /// </summary>
        /// <param name="t" type="Transaction">Transaction</param>
        /// <returns>RootNode Root</returns>
        public dynamic get_root(Transaction t) {
            return new RootNode();
        }

    }

    //https://docs.microsoft.com/en-us/dotnet/framework/interop/passing-structures?redirectedfrom=MSDN
}
