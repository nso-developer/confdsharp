﻿using System;
using System.Runtime.InteropServices;
using confdsharp.confd;

namespace confdsharp.Maapi
{
    public class Transaction
    {
        public string client_id;
        public Maapi maapi;
        private int th;
        confd_dbname db;
        public confd_trans_mode mode;

        private bool finished = false;

        /// <summary>
        /// Transaction Constructor
        /// </summary>
        /// <param name="maapi"></param>
        /// <param name="th"></param>
        /// <param name="rw"></param>
        /// <param name="db"></param>
        /// <param name="vendor"></param>
        /// <param name="product"></param>
        /// <param name="version"></param>
        /// <param name="client_id"></param>
        public Transaction(Maapi maapi, int th = 0, confd_trans_mode rw = confd_trans_mode.CONFD_READ, confd_dbname db = confd_dbname.CONFD_RUNNING,
                    string vendor = null, string product = null, string version = null, string client_id = null) {
            this.mode = rw;
            if (client_id == String.Empty) {
                this.client_id = helpers.make_client_id();
            } else {
                this.client_id = client_id;
            }
            this.db = db;
            this.maapi = maapi;
            if (th == 0) {
                this.th = libconfd.maapi_start_trans_flags2((int)maapi.msock.Handle, (int)db, (int)rw, 0, 0,
                                              vendor, product, version, client_id);
            } else {
                this.th = th;
            }
        }

        /// <summary>
        /// Get Element
        /// </summary>
        /// <param name="path">KeyPath of the element</param>
        /// <returns></returns>
        public ConfdValue get_elem(string path) {
            IntPtr v = Marshal.AllocHGlobal(constants.CONFD_VALUE_TYPE_SIZE); 
            try {           
                int r = libconfd.maapi_get_elem((int)maapi.msock.Handle, this.th,  v, path);
                helpers.confd_check_error(r, "Error in get_elem");
                var val = new ConfdValue(v);
                return val;
            } catch (Exception e) {
                Marshal.FreeHGlobal(v);
                throw e;
            }
            
        }

        /// <summary>
        /// Set Element
        /// </summary>
        /// <param name="path">KeyPath of the element</param>
        /// <param name="value">Value to set</param>
        public void set_elem(string path, dynamic value) {
            if (this.mode != confd_trans_mode.CONFD_READ_WRITE) {
                throw new InvalidOperationException("Cannot write element on a READ transaction");
            }
            IntPtr v = Marshal.AllocHGlobal(constants.CONFD_VALUE_TYPE_SIZE);
            try {
                var val = new ConfdValue(v);
                
                int r = libconfd.maapi_set_elem((int)maapi.msock.Handle, this.th, v, path);
                //helpers.confd_check_error(r, "Error in set_elem");
            } catch (Exception e) {
                Marshal.FreeHGlobal(v);
                throw e;
            }
           

        }

        /// <summary>
        /// Apply the transaction
        /// </summary>
        /// <param name="keepopen">KeepOpen</param>
        /// <param name="flags">Transaction Flags</param>
        public void apply(int keepopen =0 , int flags=0) {
            maapi.maapi_apply_trans_flags(this.th, keepopen, flags);
        }


    }

    //https://docs.microsoft.com/en-us/dotnet/framework/interop/passing-structures?redirectedfrom=MSDN
}
