﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.InteropServices;
using confdsharp.Maagic;
//https://github.com/dotnet/core/issues/756



namespace confdsharp
{


    class Program
    {



        static void Main(string[] args)
        {
            helpers.confd_init();
            var m = new Maapi.Maapi(load_schemas: true);


            var session = new Maapi.Session(m, "admin", "confdsharp");
            Console.WriteLine(session);
            var trans = m.start_write_trans();
            var address = trans.get_elem("/devices/device{test}/address");
            Console.WriteLine("Address: {0}", address.Value);
            var desc = trans.get_elem("/devices/device{test}/description");
            Console.WriteLine("Description: {0}", desc.Value);
            trans.set_elem("/devices/device{XR}/address", IPAddress.Parse("127.0.0.1"));
            
            var retries = trans.get_elem("/devices/device{test}/connect-retries/attempts").Value;
            trans.set_elem("/devices/device{test}/connect-retries/attempts", (byte)10);

            var root = m.get_root(trans); 

            foreach (confd_nsinfo nsinfo in root.namespaces) {
                //Console.WriteLine("Module: {0}, uri: {1}, prefix {2}", nsinfo.module, nsinfo.uri, nsinfo.prefix);

            }
            

            foreach (dynamic n in root) {
                Console.WriteLine(n);
            }

            /*Console.WriteLine("Dynamic Root1: {0}", root["ncs:devices"]);
            Console.WriteLine("Dynamic Root2: {0}",((dynamic)root).ncs__devices);
            Console.WriteLine("Dynamic Root3: {0}", root.c.ncs__devices); */
            Console.WriteLine("=============================");
            foreach (dynamic d in root.ncs__devices.device) {
                Console.WriteLine(d);
            }

            trans.apply();
            
        }
    }
}
