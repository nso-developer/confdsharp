# ConfDSharp

Confdsharp is a C# library that is meant to read and write a ConfD database using libconfd. It is inspired by confd maapi / maagic python APIs.

It is currently in Alpha, meaning not all features are currently bugfree or even implemented.

## Use Case Description

Confd provides C, Python, Java, ... APIs, but no C#. This project aims to fix that.

## Installation

ConfDSharp is a C# Project. You can open it with VisualStudio. This has been tested on a mac with visual studio X and dotnetcore 3.1. It should work on Linux versions of dotnet too, however this was not tested.

libconfd.so file should be either in your PATH Environment, or in the same directory of the compiled executable.
Note that the only versions of libconfd that were tested were the versions that are shipped with Cisco NSO (4.7.5 and 5.4.1 tested) and compiled to run on a Mac.

## Configuration

Currently, the versions string are hardcoded in confd/constant.cs file. 
That means you need to change the values depending of the target libconfd version.
Example:


```
//NSO 4.7.5
public const int CONFD_LIB_API_VSN = 0x06070500;
public const int MAXDEPTH = 60;
public const int MAXKEYLEN = 18;
public const int CONFD_LIB_VSN = 0x06070500;
public const string CONFD_LIB_VSN_STR = "06070500";
public const string CONFD_LIB_API_VSN_STR = "06070500";
public const int CONFD_LIB_PROTO_VSN = 67;
public const string CONFD_LIB_PROTO_VSN_STR = "67";
```

The following phython code was used to get these values
```
>>> import _ncs
>>> _ncs.LIB_API_VSN
101123328
>>> hex(_ncs.LIB_API_VSN)
'0x6070500'
>>> _ncs.LIB_API_VSN_STR
'06070500'
>>> _ncs.LIB_PROTO_VSN
67
>>> _ncs.LIB_PROTO_VSN_STR
'67'
>>> _ncs.LIB_VSN
101123328
>>> _ncs.LIB_VSN_STR
'06070500'
>>>
```

You can also change the config.cs file to adjust confd logging settings:
```
public static string confd_ipc_access_secret = "";
        public static string COONFDNET_LOGFILE = "/tmp/confd.log";
        public static confd_debug_level CONFD_DEBUG_LEVEL = confd_debug_level.CONFD_TRACE;
   
```

## Usage

program.cs shows basic usage example.
Currently, you can't do much than reading / writing data to the Confd CDB using Maapi API. 
Only ipv4-addresses, string, and int entries were tested.

```
helpers.confd_init();
var m = new Maapi.Maapi(load_schemas: true);

var session = new Maapi.Session(m, "admin", "confdsharp");
Console.WriteLine(session);
var trans = m.start_write_trans();
var address = trans.get_elem("/devices/device{XR}/address");
Console.WriteLine("Address: {0}", address.Value);
var desc = trans.get_elem("/devices/device{XR}/description");
Console.WriteLine("Description: {0}", desc.Value);
trans.set_elem("/devices/device{XR}/address", IPAddress.Parse("127.0.0.1"));

var retries = trans.get_elem("/devices/device{XR}/connect-retries/attempts").Value;
trans.set_elem("/devices/device{XR}/connect-retries/attempts", (byte)10);
trans.apply();
```

For the documentation of the above functions, you can refer to python maagic documentation in confd folder Example: confd-basic-7.3 doc/doc/api/python/confd.maapi.html

## Details about the code

There is currently 3 folders in the code:
1. confd: Contains all library bindings to libconfd.so and also a port of confd structure and constants in C#
2. Maapi: Port of some functions from Maapi API to C#
3. Maagic: Port of some functions from Maagic API to C#. Most of things here are not implemented

## Exceptions handling:

Some ConfD functions might return an error. In this case you can use the following function (defined in confd/helpers.cs):
```
public static bool confd_check_error(int code, string msg, bool checkLastError=true) {
    int marshalerr = 0;
    if (checkLastError) {
        marshalerr = Marshal.GetLastWin32Error();
    }
    if(code != 0|| marshalerr != 0) {
        throw new Exception("Code: " + code.ToString() + " Marshal Error: " + marshalerr.ToString() + " - " + msg + " - " + libconfd.confd_lasterr_string()) ;
    }
            

    return false;
}
```
The `checkLastError` setting indicate whether we should also check the system lasterror setting.

## Known issues

This is very early alpha and provided as-is. You should expect all sort of issues.

## Getting help

If you have questions, concerns, bug reports, etc., please create an issue against this repository.

## Getting involved

I am doing this on my free time and depending on the adoption by the community I may or not finish this. Any help is welcomed, please refer to:  [CONTRIBUTING](./CONTRIBUTING.md) .

## Credits and references

1. Confd Maapi / Maagic API

----

## Licensing info

This code is licensed under the GPLv3 License. See [LICENSE](./LICENSE) for details.

For information about ConfD licenses, please refer to [https://www.tail-f.com/confd-basic/](https://www.tail-f.com/confd-basic/)

----
